package com.victorlapin.tiles.sound

import android.content.Intent
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import android.content.pm.PackageManager


class GoogleSoundSearchTileService : TileService() {
    override fun onClick() {
        unlockAndRun {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.action = "$GOOGLE_APP_PACKAGE.MUSIC_SEARCH"
            startActivity(intent)
        }
    }

    override fun onStartListening() {
        qsTile.state = if (isGoogleAppInstalled()) Tile.STATE_ACTIVE else Tile.STATE_UNAVAILABLE
        qsTile.updateTile()
    }

    private fun isGoogleAppInstalled(): Boolean = try {
        val info = packageManager
                .getPackageInfo(GOOGLE_APP_PACKAGE, PackageManager.GET_META_DATA)
        info != null
    } catch (e: PackageManager.NameNotFoundException) {
        false
    }

    companion object {
        private const val GOOGLE_APP_PACKAGE = "com.google.android.googlequicksearchbox"
    }
}